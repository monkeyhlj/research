package user_answer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SelectAnswerServlet
 */
public class SelectAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			List<UserAnswer> list=DataBaseOperate.selectAnswer();
			request.setAttribute("list", list);
			/*for (UserAnswer ua : list) {
				List<UserOption> list1=ua.getList_o();
				for (UserOption uo : list1) {
					System.out.println(uo.getOption());
					System.out.println(uo.getQueid());
					System.out.println(uo.getUserId());
					System.out.println(uo.getUsername());
				}
			}*/
			request.getRequestDispatcher("Role/index1.jsp").forward(request,response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
