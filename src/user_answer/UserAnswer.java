package user_answer;

import java.util.List;

public class UserAnswer {
	private String userId;//用户id
	private String userName;//用户名
	private List<UserOption> list_o;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<UserOption> getList_o() {
		return list_o;
	}
	public void setList_o(List<UserOption> list_o) {
		this.list_o = list_o;
	}
	public UserAnswer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserAnswer(String userId, String userName, List<UserOption> list_o) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.list_o = list_o;
	}
	
	
	
}
