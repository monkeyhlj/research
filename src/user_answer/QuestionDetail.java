package user_answer;

import java.util.List;
//详细描述整个问题的一个类，其选项存放在list，一个选项代表了一个list中的一个QuesOption类
public class QuestionDetail {
	private  String quesid;//问题题号
	private String ques;//问题详情
	private List<QuesOption> list;//储存问题的四个选项
	public String getQuesid() {
		return quesid;
	}
	public void setQuesid(String quesid) {
		this.quesid = quesid;
	}
	public String getQues() {
		return ques;
	}
	public void setQues(String ques) {
		this.ques = ques;
	}
	public List<QuesOption> getList() {
		return list;
	}
	
	public QuestionDetail(String quesid, String ques, List<QuesOption> list) {
		super();
		this.quesid = quesid;
		this.ques = ques;
		this.list = list;
	}
	public QuestionDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setList(List<QuesOption> list) {
		this.list = list;
	}
	
}
