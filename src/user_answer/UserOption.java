package user_answer;

public class UserOption {
	private String userId;// �û�Id
	private String username;
	private String queid;// ���Id
	private String option;

	public  String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getQueid() {
		return queid;
	}

	public void setQueid(String queid) {
		this.queid = queid;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserOption(String userId, String username, String queid, String option) {
		super();
		this.userId = userId;
		this.username = username;
		this.queid = queid;
		this.option = option;
	}

	public UserOption() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserOption(String userId, String queid, String option) {
		super();
		this.userId = userId;
		this.queid = queid;
		this.option = option;
	}

	public UserOption(String queid, String option) {
		super();
		this.queid = queid;
		this.option = option;
	}

}
