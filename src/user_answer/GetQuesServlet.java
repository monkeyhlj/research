package user_answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetQuesServlet
 */
public class GetQuesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) {
		try {		
			Connection conn = DataBaseOperate.linkDB();
			//*
			String sql1 = "select * from question";
			PreparedStatement ps2 = conn.prepareStatement(sql1);
			ResultSet resultSet = ps2.executeQuery();
			ArrayList<QuestionDetail> items = new ArrayList<QuestionDetail>();
			while(resultSet.next()){
				String quesid = resultSet.getString("queid");
				String que = resultSet.getString("que");
				Connection con=DataBaseOperate.linkDB();
				String sql = "select * from answer where queid = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, quesid);
				ResultSet rs1 = ps.executeQuery();
				ArrayList<QuesOption> answers = new ArrayList<QuesOption>();
				while(rs1.next()){
					QuesOption answer = new QuesOption();
					answer.setQuesid(quesid);
					answer.setAnid(rs1.getString("anid"));
					answer.setAns(rs1.getString("ans"));
					answers.add(answer);
				}
			QuestionDetail item = new QuestionDetail(quesid, que, answers);
			items.add(item);
			}
			request.setAttribute("items", items);
			RequestDispatcher rd = request.getRequestDispatcher("main.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
}
