package question;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import question.dao.AnswerDao;
import question.dao.QuestionDao;

/**
 * Servlet implementation class UpdateQuestionServlet
 */
public class UpdateQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String queid = request.getParameter("queid");
		String que = request.getParameter("que");
		QuestionDao qd = new QuestionDao();
		AnswerDao ad = new AnswerDao();
		Map<String, String[]> map = request.getParameterMap();
		Set<String> set = map.keySet();
		try {
			for (String key : set) {
				String[] values = map.get(key);
				for (String value : values) {
					ad.updateAnwser(key, value);
				}
			}
			qd.updateQuestion(queid, que);
			response.sendRedirect("QueryQuestion");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
