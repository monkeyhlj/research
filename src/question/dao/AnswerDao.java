package question.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import question.pojo.Answer;
import question.service.DBUtils;

public class AnswerDao {
	
	//���Ӵ�
	public void insertAnswer(String queid,String anid,String ans) throws Exception{
		
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql="insert into answer values(?,?,?)";
		PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1, queid);
		pre.setString(2, anid);
		pre.setString(3, ans);
		pre.execute();
		con.close();
	}
	
	//������������ɾ����
	public void deleteAnswers(String queid) throws Exception {
	
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "delete from answer where queid=?";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, queid);
		boolean res = pre.execute();
		con.close();
	}
	
	//ɾ�����д�
	public void deleteAllAns() throws Exception {
	
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "delete * from answer";
		PreparedStatement pre = con.prepareStatement(sql);
		ResultSet res = pre.executeQuery();
		con.close();
	}
	
	//�޸Ĵ�
	public void updateAnwser(String anid,String ans) throws Exception {
		
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql="update answer set ans=? where anid=?";
		java.sql.PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1,ans);
		pre.setString(2,anid);
		pre.execute();
		con.close();
	}
	
	//�����������Ų�ѯ��Ӧ�Ĵ�
	public List<Answer> selectAnswers(String queid) throws Exception {
	
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "select * from answer where queid=?";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, queid);
		ResultSet res = pre.executeQuery();
		List<Answer> alist = new ArrayList<Answer>();
		while (res.next()) {
			String ansid = res.getString(2);
			String ans = res.getString(3);
			Answer answer = new Answer(ansid, ans);
			alist.add(answer);
		}
		con.close();
		return alist;
	}
	
	//��ѯ�����еĴ�
	public List<Answer> selectAllAns() throws Exception {
		
		//���ݿ�����
		Connection con = DBUtils.getCon();
		
		// ִ��sql���
		String sql = "select * from answer";
		PreparedStatement pre = con.prepareStatement(sql);
		ResultSet res = pre.executeQuery();
		List<Answer> alist = new ArrayList<Answer>();
		while (res.next()) {
			String ansid = res.getString(2);
			String ans = res.getString(3);
			Answer answer = new Answer(ansid, ans);
			alist.add(answer);
		}
		con.close();
		return alist;
	}	
}
