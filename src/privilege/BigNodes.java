package privilege;

import java.util.List;

public class BigNodes {
	private String id;
	private List<MiddleNodes> menu;
	public BigNodes() {
		super();
	}
	public BigNodes(String id, List<MiddleNodes> menu) {
		super();
		this.id = id;
		this.menu = menu;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<MiddleNodes> getMenu() {
		return menu;
	}
	public void setMenu(List<MiddleNodes> menu) {
		this.menu = menu;
	}
	
}
