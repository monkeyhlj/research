package admin;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import admin.TestPro;

public class ManagerJdbc {
	public List<Manager> selectByManagerID(String Managerid) throws Exception{
		String sql="select * from manager where mgid=?";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1, Managerid);
		ResultSet res=pre.executeQuery();
		List<Manager> list=new ArrayList<Manager>();
		while(res.next()){
			String id1=res.getString(1);
			String name1=res.getString(2);
			String psw1=res.getString(3);
			Manager u=new Manager(id1,name1,psw1);
			list.add(u);
		}
		return list;
	}
	public static boolean selectManager(String Managerid) throws Exception{
		String sql="select * from manager where mgid=?";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1, Managerid);
		ResultSet res=pre.executeQuery();
		List<Manager> list=new ArrayList<Manager>();
		if(res.next()){
			return false;
		}
		return true;
	}
	public void saveManager(String id,String name,String psw) throws Exception{
		String sql="insert into manager values(?,?,?)";
		Class.forName(TestPro.readProper("name"));
		String url=TestPro.readProper("url");
		java.sql.Connection con = DriverManager.getConnection(url, TestPro.readProper("username"), TestPro.readProper("psw"));
		PreparedStatement pre=con.prepareStatement(sql);
		pre.setString(1, id);
		pre.setString(2, name);
		pre.setString(3, psw);
		pre.execute();
		con.close();
	}
}
