package admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateAdminServlet
 */
public class UpdateAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String id=request.getParameter("iid");
		String name=request.getParameter("uname");
		String psw=request.getParameter("psw");
		Adminjdbc qu=new Adminjdbc();
		try{
			qu.updateAdmin(id,name,psw);
			response.sendRedirect("QueryAdmin");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
