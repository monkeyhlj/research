package information;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PswReviseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String password=request.getParameter("password");
		String id=request.getParameter("id");
		InformationJdbc Information=new InformationJdbc();
		try{
			Information.UpdatePswById(id,password);
			response.sendRedirect("Public/login1.jsp");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
