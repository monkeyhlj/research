package regist;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import information.Information;
import information.InformationJdbc;

public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String userid = request.getParameter("userid");
		String uname = request.getParameter("username");
		//System.out.println(uname);
		String password = request.getParameter("password");
		String sex = request.getParameter("sex");
		String a=request.getParameter("age");
		int age=Integer.parseInt(a);
		//System.out.println(age);
		String year=request.getParameter("year");
		String month=request.getParameter("month");
		String day=request.getParameter("day");
		InformationJdbc q=new InformationJdbc();
		try{
			q.InsertBirthday(userid,uname,year,month,day,sex,age,password);
		}catch(Exception e){
			e.printStackTrace()  ;
		} 
//		System.out.println(uname + "==" + password1);
//		if(uname==""){
//			//System.out.println("閿熺煫浼欐嫹閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷蜂负閿熸枻鎷�");
//			RequestDispatcher dis=request.getRequestDispatcher("dreg.jsp");
//			dis.forward(request, response);
//			return;
//		}
/*		if(sex==""){
			//System.out.println("閿熺殕鎲嬫嫹");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg12.jsp");
			dis.forward(request, response);
			return;
		}
		if(age==""){
			//System.out.println("閿熸枻鎷烽敓鏂ゆ嫹");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg13.jsp");
			dis.forward(request, response);
			return;
		}
		if(userid==""){
			//System.out.println("閿熺晫璇濋敓鑴氳鎷烽敓鏂ゆ嫹涓洪敓鏂ゆ嫹");
			RequestDispatcher dis=request.getRequestDispatcher("rf/drep11.jsp");
			dis.forward(request, response);
			return;
		}
		uname=uname.replace(" ","");
		if("".equals(uname)){
			//System.out.println("閿熺煫浼欐嫹閿熸枻鎷烽敓鏂ゆ嫹寮忛敓鏂ゆ嫹閿熸枻鎷�");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg2.jsp");
			dis.forward(request, response);
			return;
		}
		if(uname.length()>6){
			//System.out.println("閿熺煫浼欐嫹閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷�");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg3.jsp");
			dis.forward(request, response);
			return;
		}
		if(password==""){
			//System.out.println("閿熸枻鎷烽敓璇笉閿熸枻鎷蜂负閿熸枻鎷�");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg4.jsp");
			dis.forward(request, response);
			return;
		}
		password=password.replace(" ","");
		if("".equals(password)){
			//System.out.println("閿熸枻鎷烽敓鏂ゆ嫹閿熺粸鏂ゆ嫹閿熸枻鎷烽敓锟�");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg5.jsp");
			dis.forward(request, response);
			return;
		}
		if(password.length()>16){
			//System.out.println("閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷烽敓锟�");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg6.jsp");
			dis.forward(request, response);
			return;
		}*/
//		if(!password1.equals(password2)) {
//			//System.out.println("閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷烽敓璇笉涓�閿熸枻鎷�");
//			RequestDispatcher dis=request.getRequestDispatcher("dreg14.jsp");
//			dis.forward(request, response);
//			return;
//		}
		
		boolean t = false;
		try {
			t = checkUserId(userid);
		} catch (Exception e1) {
			
			e1.printStackTrace();
		}
		if(t){
			//System.out.println("閿熸枻鎷烽敓鐭紮鎷烽敓绐栬揪鎷烽敓鏂ゆ嫹");
			RequestDispatcher dis=request.getRequestDispatcher("rf/dreg7.jsp");
			dis.forward(request, response);
			return;
		}
		else {
		String sql = "insert into user values(?,?,?,?,?)";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/research?characterEncoding=utf-8";
			Connection con = DriverManager.getConnection(url, "root", "123456");
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, userid);
			pre.setString(2, uname);
			pre.setString(3, password);
			pre.setString(4, sex);
			pre.setString(5, a);
			//System.out.println(sex);
			//System.out.println(age);
			pre.execute();
			con.close();
			request.setAttribute("str", pre);
			InformationJdbc question=new InformationJdbc();
			try{
				List<Information> infoList=question.queryInfoById(userid);
				request.setAttribute("infoList",infoList);
				RequestDispatcher dis=request.getRequestDispatcher("Security/index.jsp");
				dis.forward(request, response);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
	}
		}
	}

	private boolean checkUserId(String userid) throws Exception {
		String sql="select * from user where userid =?";
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/research?characterEncoding=utf-8";
		Connection con = DriverManager.getConnection(url, "root", "123456");
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1,userid);
		ResultSet res=pre.executeQuery();
		boolean t=res.next();
		return t;
	
	}

}
