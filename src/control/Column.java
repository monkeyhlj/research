package control;

public class Column {
	String cid;
	String cname;
	String curl;
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCurl() {
		return curl;
	}
	public void setCurl(String curl) {
		this.curl = curl;
	}
	public Column(String cid, String cname, String curl){
		super();
		this.cid = cid;
		this.cname = cname;
		this.curl = curl;
	}
	
	public Column(String cid, String cname) {
		super();
		this.cid = cid;
		this.cname = cname;
	}
	
	
}
