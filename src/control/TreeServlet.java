package control;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import privilege.LittleNodes;


public class TreeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		List<Node> list=new ArrayList<Node>();
		List<Node> children=new ArrayList<Node>();
		
//		Middlejdbc m=new Middlejdbc();
//		try {
//			List<Column> c=m.queryColu();
//			Node l=new Node();
//			for(int i=0;i<list.size();i++)
//			{
//				Column mu=c.get(i);
//				l=new Node(mu.getCid(),mu.getCname(),null,null);
//				children.add(l);
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		Node node2=new Node("1", "权限设置",null,null);
		Node node3=new Node("2", "结果查看",null,null);
		Node node4=new Node("3", "角色管理",null,null);
		Node node5=new Node("4", "用户管理",null,null);
		Node node6=new Node("5", "题目管理",null,null);
		children.add(node2);
		children.add(node3);
		children.add(node4);
		children.add(node5);
		children.add(node6);
		
		Node node1=new Node("0", "系统管理","open",children);
		list.add(node1);
		
		
		JSONArray json=JSONArray.fromObject(list);//将list->转换为json格式数据
		PrintWriter pw=response.getWriter();
		pw.println(json.toString());
		pw.close();
	}

}
