package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UpdateMiddleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String id=request.getParameter("id");
		String mgid=request.getParameter("mgid");
		//System.out.println(mgid);
		//System.out.println(id);
		String[] s=id.split(",");
		Middlejdbc m=new Middlejdbc();
		try{
			for(int i=0;i<s.length;i++)
			{
				m.saveMiddle(mgid, s[i]);
			}
			RequestDispatcher dis=request.getRequestDispatcher("QueryMiddle");
			dis.forward(request, response);	
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
