package control;

public class Power {
	private String cid;
	private String cname;
	private String url;
	public Power() {
		super();
	}
	public Power(String cid, String cname, String url) {
		super();
		this.cid = cid;
		this.cname = cname;
		this.url = url;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
