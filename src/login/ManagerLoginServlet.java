package login;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManagerLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String id=request.getParameter("id"); 
		//System.out.println(id);
		String psw=request.getParameter("psw"); 
		//System.out.println(name);
	
		try {
			String sql="select * from manager where mgid=? and mgpsw=?";	
			Class.forName("com.mysql.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/research?characterEncoding=utf-8";
			Connection con=DriverManager.getConnection(url, "root", "123456");
			PreparedStatement pre=con.prepareStatement(sql);
			pre.setString(1, id);
			pre.setString(2, psw);
			ResultSet res=pre.executeQuery();
			boolean t=res.next();
			//System.out.println(t);
			if(t){
				request.setAttribute("mgid", id);
				RequestDispatcher dis=request.getRequestDispatcher("Msuccess.jsp");
				dis.forward(request, response);	
			}
			else{
				RequestDispatcher dis=request.getRequestDispatcher("Mfail.jsp");
				dis.forward(request, response);
			}
			con.close();
			
		} catch (Exception e) {		
			e.printStackTrace();
		} 
	}

}
