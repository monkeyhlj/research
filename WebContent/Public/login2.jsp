<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>登录页</title>
		<link rel="stylesheet" type="text/css" href="../Css/logincss.css" />
	</head>
	<body>
		<header>
			<img src="../img/logo.png" />
			<ul>
				<li><a href="index1.jsp">首页</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">填写问卷</a></li>
				<li><a href="javascript:alert('请先登录或注册！');">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.jsp">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
		</header>
		<div id="log">
			<div id="logg">
					<p><a href="login1.jsp">用户登录</a></p>
					<p>|</p>
					<p><a href="login2.jsp" style="color: red;">管理员登录</a></p>
				</div>
			<form action="http://localhost:8080/research/managerLoginServlet" method="post">
				<input id="username" name="id" type="tel" maxlength="11" placeholder="  请输入电话号码" />
				<input id="password" name="psw" type="password" maxlength="10" placeholder="  请输入密码" />
				<input id="denglu" type="submit" value="登录" />
			</form>
			<p id="cp3">忘记密码 | 找回密码</p>
		</div>
		<footer>
			<p>HXPZLQ版权所有</p>
		</footer>
	</body>
</html>
