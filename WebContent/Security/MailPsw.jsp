<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="Css/logincss.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>邮箱验证</title>
</head>
<style>
	.input{
				font-size: 18px;
				width: 260px;
				height: 40px;
				border: none;
				border-radius: 10px;
				margin-top: 40px;
				margin-left: 65px;
				border: 1px solid #e2e2e2;
			}
</style>
<body>
<header>
			<img src="img/logo.png" />
			<ul>
				<li><a href="Public/index1.html">首页</a></li>
				<li><a href="#">个人中心</a></li>
				<li><a href="#">填写问卷</a></li>
				<li><a href="#">联系我们</a></li>
			</ul>
			<div id="lr">
				<a href="login1.html">登录</a>
				<p>|</p>
				<a href="regist.jsp">注册</a>
			</div>
	</header>
<div id="log">
	<div id="logg">
			<p><a>邮箱验证</a></p>
	</div>
	<form action="http://localhost:8080/research/PswServlet" method="post" id="form">
		<c:forEach items="${infoList }" var="info"> 
			<input class="input" value="${id }" name="id" type="hidden">
			<input class="input" value="${info.email }" name="email" readonly="readonly">
			<input name="code" id="correctcode" type="hidden" value="${code }">
			<input class="input" name="code" id="code" placeholder="请输入验证码">
			<input type="button" value="确定" onclick="check()" id="denglu">
		</c:forEach>
	</form>
</body>
<script>
function check(){
	var code=document.getElementById('code').value;
	var correctcode=document.getElementById('correctcode').value;
	if(code==''){
		alert("验证码不能为空！");
		return;
	}else if(code==correctcode){
		alert("验证码正确！");
		var form=document.getElementById('form');
		form.submit();
		return;
	}else{
		alert("验证码错误！");
		return;
	}
}
</script>
</html>