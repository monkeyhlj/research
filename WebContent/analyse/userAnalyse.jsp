<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="analyse.SessionListener"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://"
+ request.getServerName() + ":" + request.getServerPort()
+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
 <script type="text/javascript" src="analyse/jschart.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户数据分析</title>
</head>
<style>
#picture{
	margin-left:520px;
	margin-top:-330px;
}
</style>
<body onload="load()">
当前在线人数为<%=SessionListener.getCount()%><br><br>
<a style="color:gray;">本问卷调查系统共有${countmannum}个用户，其中${man }%的用户为男性，${woman }%的用户为女性</a>
<input value="${countman}" id="countman" type="hidden">
<input value="${countwoman}" id="countwoman" type="hidden">

<input value="${countteenager}" id="countman" type="hidden">
<input value="${countadult}" id="countwoman" type="hidden">
<input value="${countold}" id="countman" type="hidden">
<input value="${countwoman}" id="countwoman" type="hidden">
<br>
<br>
<div id="graph">Loading graph...</div>
<div id="picture">
	<div id="graph1">Loading graph...</div>
</div>
<a style="color:gray;">其中${teenager }%的用户为0-18岁的青少年，${adult }%的用户为18-50的成年人，${old }%为50以上的中老年用户</a>
<div id="graph2">Loading graph...</div>
<div id="picture">
	<div id="graph3">Loading graph...</div>
</div>
<script type="text/javascript">
function load(){
//饼状
	var countman=document.getElementById('countman').value;
	var countwoman=document.getElementById('countwoman').value;
	var countmanValue=parseInt(countman);
	var countwomanValue=parseInt(countwoman);
	var myData = new Array(['男',countmanValue], ['女', countwomanValue]);
	var colors = ['#C40000', '#F9ECA2'];
	var myChart = new JSChart('graph', 'pie');
	myChart.setDataArray(myData);
	myChart.colorizePie(colors);
	myChart.setTitle('用户性别比例');
	myChart.setTitleColor('#8E8E8E');
	myChart.setTitleFontSize(11);
	myChart.setTextPaddingTop(30);
	myChart.setSize(500, 321);
	myChart.setPiePosition(308, 170);
	myChart.setPieRadius(85);
	myChart.setPieUnitsColor('#555');
	myChart.draw();
//条形	
	var myData = new Array(['男', countmanValue], ['女', countwomanValue]);
	var colors = ['#AF0202',  '#81C714'];
	var myChart = new JSChart('graph1', 'bar');
	myChart.setDataArray(myData);
	myChart.colorizeBars(colors);
	myChart.setTitle('用户性别比例');
	myChart.setTitleColor('#8E8E8E');
	myChart.setAxisNameX('');
	myChart.setAxisNameY('');
	myChart.setAxisColor('#C4C4C4');
	myChart.setAxisNameFontSize(16);
	myChart.setAxisNameColor('#999');
	myChart.setAxisValuesColor('#7E7E7E');
	myChart.setBarValuesColor('#7E7E7E');
	myChart.setAxisPaddingTop(60);
	myChart.setAxisPaddingRight(140);
	myChart.setAxisPaddingLeft(180);
	myChart.setAxisPaddingBottom(40);
	myChart.setTextPaddingLeft(150);
	myChart.setTitleFontSize(11);
	myChart.setBarBorderWidth(1);
	myChart.setBarBorderColor('#C4C4C4');
	myChart.setBarSpacingRatio(50);
	myChart.setGrid(false);
	myChart.setSize(500, 321);
	myChart.draw();
	
//饼图
	var myData = new Array(['青少年',2], ['成年人', 5],['中老年', 3]);
	var colors = ['#C40000', '#F9ECA2','#FA5400'];
	var myChart = new JSChart('graph2', 'pie');
	myChart.setDataArray(myData);
	myChart.colorizePie(colors);
	myChart.setTitle('用户年龄比例');
	myChart.setTitleColor('#8E8E8E');
	myChart.setTitleFontSize(11);
	myChart.setTextPaddingTop(30);
	myChart.setSize(500, 321);
	myChart.setPiePosition(308, 170);
	myChart.setPieRadius(85);
	myChart.setPieUnitsColor('#555');
	myChart.draw();
//折线图
	var myData = new Array(['青少年',2], ['成年人', 5],['中老年', 3]);
	var myChart = new JSChart('graph3', 'line');
	myChart.setDataArray(myData);
	myChart.setTitle('用户年龄');
	myChart.setTitleColor('#8E8E8E');
	myChart.setTitleFontSize(11);
	myChart.setAxisNameX('');
	myChart.setAxisNameY('');
	myChart.setAxisColor('#C4C4C4');
	myChart.setAxisValuesColor('#343434');
	myChart.setAxisPaddingLeft(100);
	myChart.setAxisPaddingRight(120);
	myChart.setAxisPaddingTop(50);
	myChart.setAxisPaddingBottom(40);
	myChart.setAxisValuesNumberX(6);
	myChart.setGraphExtend(true);
	myChart.setGridColor('#c2c2c2');
	myChart.setLineWidth(6);
	myChart.setLineColor('#9F0505');
	myChart.setSize(616, 321);
	myChart.setBackgroundImage('chart_bg.jpg');
	myChart.draw();
}
</script>
</body>
</html>