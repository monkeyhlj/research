package analyse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import information.InformationJdbc;

public class UserAnalyse {
	public String CountMan() throws Exception {
		String sql="select count(*) from user where sex='男'";
		Connection con=InformationJdbc.getCon();
		PreparedStatement pre=con.prepareStatement(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countman=res.getString(1);
		con.close();
		return countman;
	}
	public String CountWoman() throws Exception {
		String sql="select count(*) from user where sex='女'";
		Connection con=InformationJdbc.getCon();
		PreparedStatement pre=con.prepareStatement(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countman=res.getString(1);
		con.close();
		return countman;
	}
	public String CountTeenager() throws Exception {
		String sql="select count(*) from information where age<18&&age>0";
		Connection con=InformationJdbc.getCon();
		PreparedStatement pre=con.prepareStatement(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countteenager=res.getString(1);
		con.close();
		return countteenager;
	}
	public String CountAdult() throws Exception {
		String sql="select count(*) from information where age>=18&&age<50";
		Connection con=InformationJdbc.getCon();
		PreparedStatement pre=con.prepareStatement(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countadult=res.getString(1);
		con.close();
		return countadult;
	}
	public String CountOld() throws Exception {
		String sql="select count(*) from information where age>=50";
		Connection con=InformationJdbc.getCon();
		PreparedStatement pre=con.prepareStatement(sql);
		pre.execute();
		ResultSet res=pre.executeQuery();
		res.next();
		String countold=res.getString(1);
		con.close();
		return countold;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
