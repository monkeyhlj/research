package analyse;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UserAnalyseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		UserAnalyse ua=new UserAnalyse();
		try {
			//性别
			String countman=ua.CountMan();
			String countwoman=ua.CountWoman();
			int countmanValue=Integer.parseInt(countman);
			int countwomanValue=Integer.parseInt(countwoman);
			int countmannum=countmanValue+countwomanValue;
			int man=(100*countmanValue)/countmannum;
			int woman=100-man;
			request.setAttribute("countman", countmanValue);
			request.setAttribute("countwoman", countwomanValue);
			request.setAttribute("countmannum", countmannum);
			request.setAttribute("man", man);
			request.setAttribute("woman", woman);
			//年龄
			String countteenager=ua.CountTeenager();
			String countadult=ua.CountAdult();
			String countold=ua.CountOld();
			int countteenagerValue=Integer.parseInt(countteenager);
			int countadultValue=Integer.parseInt(countadult);
			int countoldValue=Integer.parseInt(countold);
			int countage=countteenagerValue+countadultValue+countoldValue;
			int teenager=(100*countteenagerValue)/countage;
			int adult=(100*countadultValue)/countage;
			int old=100-teenager-adult;
			request.setAttribute("countteenager", countteenagerValue);
			request.setAttribute("countadult", countadultValue);
			request.setAttribute("countold", countoldValue);
			request.setAttribute("teenager", teenager);
			request.setAttribute("adult", adult);
			request.setAttribute("old", old);
			RequestDispatcher dis=request.getRequestDispatcher("analyse/userAnalyse.jsp");
			dis.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
