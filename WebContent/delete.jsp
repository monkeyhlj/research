<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<title>CheckBox Tree - jQuery EasyUI Demo</title>
<link rel="stylesheet" type="text/css" href="themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="themes/icon.css">
<link rel="stylesheet" type="text/css" href="Css/demo.css">
<script type="text/javascript" src="Js/jquery.min.js"></script>
<script type="text/javascript" src="Js/jquery.easyui.min.js"></script>
</head>
<body>

<%
	String mgid = request.getParameter("mgid");
%>
<form method="post">
	<div style="margin: 20px 0;">
		
		<a  type="hidden" href="#" class="easyui-linkbutton" onclick="getChecked()">GetChecked</a>
	</div>

	<div style="margin: 10px 0">
		<input type="checkbox" 
			onchange="$('#tt').tree({cascadeCheck:$(this).is(':checked')})">CascadeCheck
		<input type="checkbox" checked
			onchange="$('#tt').tree({onlyLeafCheck:$(this).is(':checked')})">OnlyLeafCheck
	</div>

	<div class="easyui-panel" style="padding: 5px">
		<ul id="tt" class="easyui-tree"
			data-options="url:'Tree',method:'get',animate:true,checkbox:true"></ul>
	</div>
	
<input onclick="getChecked()" type="button" value="保存设置" style="width:100px;height:30px;border-radius:5px;margin-top:10px;" />

	<script type="text/javascript">
			function getChecked() {
				var nodes = $('#tt').tree('getChecked');
				var s = '';
				for (var i = 0; i < nodes.length; i++) {
					if (s != ''){
						s += ',';
					}
					s += nodes[i].id;
				}
				window.location.href = "http://localhost:8080/research/DeletePowerServlet?mgid=<%=mgid%>&cid="+s;
				//document.getElementById("uid").innerHTML="<input name='uid' value='"+s+"'/>"
			}
	</script>
</form>
</body>
</html>