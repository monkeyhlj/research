<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>CheckBox Tree - jQuery EasyUI Demo</title>
	<link rel="stylesheet" type="text/css" href="themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="themes/icon.css">
	<link rel="stylesheet" type="text/css" href="Css/demo.css">
	<script type="text/javascript" src="Js/jquery.min.js"></script>
	<script type="text/javascript" src="Js/jquery.easyui.min.js"></script>
</head>
<body>
	<%
		String val=(String) request.getParameter("mgid");
		//HttpSession session=request.getSession();//获取session，获取不到就自己创建一个session，存在于服务器端
		//session.setAttribute("mgid", val);
	%>
	<h2>修改权限值</h2>
	<div style="margin:20px 0;">
		<input style="width:80px;height:30px" type="button" class="easyui-linkbutton" value="保存权限" onclick="getChecked()"/>
	</div>
	<div style="margin:10px 0">
		<input type="checkbox" checked onchange="$('#tt').tree({cascadeCheck:$(this).is(':checked')})">CascadeCheck 
		<input type="checkbox"  onchange="$('#tt').tree({onlyLeafCheck:$(this).is(':checked')})">OnlyLeafCheck
	</div>
	<div class="easyui-panel" style="padding:5px">
		<ul id="tt" class="easyui-tree" data-options="url:'Tree',method:'get',animate:true,checkbox:true"></ul> <!-- come from TreeSeverlet -->
	</div>
	<script type="text/javascript">
		function getChecked(){
			var nodes = $('#tt').tree('getChecked');
			var s = '';
			for(var i=0; i<nodes.length; i++){
				if (s != '') s += ',';
				s += nodes[i].id;
			}
			//alert(s);
			window.location.href="http://localhost:8080/research/UpdateMiddle?mgid=<%=val%>&id="+s;
		}
	</script>
</body>
</html>